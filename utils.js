const { join } = require('path')
const { fileLoader, mergeTypes } = require('merge-graphql-schemas')

function getMergedSchema (dir, includeBase = true) {
  const baseTypes = includeBase
    ? fileLoader(join(dir, '../base/'))
    : []

  const typesArray = fileLoader(join(dir, './'))
  return mergeTypes([...baseTypes, ...typesArray], { all: true })
}

Object.assign(exports, {
  getMergedSchema,
})
