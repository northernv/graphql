## @northernv/graphql

[![npm](https://img.shields.io/npm/v/@northernv/dataloader.svg?maxAge=2592000)](https://www.npmjs.com/package/@northernv/dataloader)
[![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/shanestillwell/dataloader/raw/master/LICENSE)

## Usage

```
npm install -S @northernv/graphql;
```

```
yarn add @northernv/graphql;
```
# Schema for ...

## Schema

* Queries and Mutations are in `queries.gql`
* Types are in `types.gql`

## Templates 

The client query/mutation/fragments are held in `/t`. These are used directly by the server when creating contract (integration) tests for the various GQL methods.

## Client GQL files

Run the `npm run build` file to generate the frontend GQL files.

### /static

Files ready to be consumed by React/Vue when using [Apollo GraphQL loader](https://www.apollographql.com/docs/react/recipes/webpack.html)

#### Usage

In Webpack
```
loaders: [
  {
    test: /\.(graphql|gql)$/,
    exclude: /node_modules/,
    loader: 'graphql-tag/loader'
  }
]
```

In the file
```
import Unit from '@northernv/vgql/static/unit.gql'

const { data, errors } = await this.$apollo.query({
  query: Unit,
  variables: {
    input: { ... },
  }
})

```

### /json 
Files ready to be consumed by Apollo, no need for graphql-tag

#### Usage

```
import Unit from '@northernv/vgql/json/unit'

const { data, errors } = await this.$apollo.query({
  query: Unit,
  variables: {
    input: { ... },
  }
})

```

### /gql

Files used with graphql-tag

#### Usage

```
import Unit from '@northernv/vgql/gql/unit'
import gql from 'graphql-tag'

const { data, errors } = await this.$apollo.query({
  query: gql`Unit`,
  variables: {
    input: { ... },
  }
})

```

## Directives

* `@timestamp`: Adds `createdAt`, `updatedAt`, `deletedAt` properties to types `timestamp-directive.js`
In your models, that are Javascript classes, include a static function called `loaders` that returns an object of dataloaders

