const h = require('../index')

test('resolveTemplates fails for duplicate keys', () => {
  expect(() => {
    h.resolveTemplates({ key: 'v' }, { key: 'dup' })
  }).toThrow()
})
