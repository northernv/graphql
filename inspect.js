#!/usr/bin/env node

/* eslint-disable */

const { default: privateTypeDefs } = require('./private')
const { default: publicTypeDefs } = require('./public')

const args = process.argv
const flag = args[args.length - 1]
const isPublic = flag === '--public'
const label = isPublic ? 'Public' : 'Private'

const heading = `
#########################################
######          ${label}            ######
#########################################
`

console.log(heading)
console.log(isPublic ? publicTypeDefs : privateTypeDefs)
console.log(heading)
