const { fileLoader, mergeTypes } = require('merge-graphql-schemas')

function resolveTemplates (entries, fragments) {
  return entries.reduce((memo, val) => {
    memo[val[0]] = val[1](fragments)
    return memo
  }, {})
}

function fragments (templates = []) {
  return templates.reduce((memo, { fragments }) => {
    if (!fragments) return memo
    return merge(memo, fragments)
  }, {})
}

function clientQueries (templates = [], clientFragments) {
  return templates.reduce((memo, { queries }) => {
    if (!queries) return memo
    const q = resolveTemplates(Object.entries(queries), clientFragments)
    return merge(memo, q)
  }, {})
}

function clientMutations (templates = [], clientFragments) {
  return templates.reduce((memo, { mutations }) => {
    if (!mutations) return memo
    const m = resolveTemplates(Object.entries(mutations), clientFragments)
    return merge(memo, m)
  }, {})
}

function merge (source, addon) {
  const keys = Object.keys(addon)
  return keys.reduce((memo, key) => {
    if (memo[key] !== undefined) throw new Error(`${key} already exists`)
    memo[key] = addon[key]
    return memo
  }, source)
}

function getMergedSchema (dir, all = true) {
  const typesArray = fileLoader(dir)
  return mergeTypes(typesArray, { all })
}

Object.assign(exports, {
  getMergedSchema,
  fragments,
  clientQueries,
  clientMutations,
  resolveTemplates,
  merge,
})
